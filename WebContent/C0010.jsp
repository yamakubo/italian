<%@page import="java.util.List"%>
<%@page contentType="text/html; charset=UTF-8" %>
<% List<String> errors = (List<String>)request.getAttribute("loginErrors"); %>
<!DOCTYPE html>
<html lang="ja">
  <head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <!-- The above 3 meta tags *must* come first in the head; any other head content must come *after* these tags -->
    <title>ログイン</title>

    <!-- Bootstrap -->
<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css" integrity="sha384-BVYiiSIFeK1dGmJRAkycuHAHRg32OmUcww7on3RYdg4Va+PmSTsz/K68vbdEjh4u" crossorigin="anonymous">

    <!-- HTML5 shim and Respond.js for IE8 support of HTML5 elements and media queries -->
    <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
    <!--[if lt IE 9]>
      <script src="https://oss.maxcdn.com/html5shiv/3.7.3/html5shiv.min.js"></script>
      <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
    <![endif]-->
    <style>
    	body{
    		background-color: #eee;
    		padding-top: 90px;
    		white-space: nowrap;
    		}
    </style>
  </head>


<body>
	<form method="POST" action="C0010">
	<input type="hidden" name="pg" value="C0020">
	<% if(errors != null && errors.size() != 0){ %>
	<div class="container">
			<div class="row">
				<div class="alert alert-danger" role="alert">
  					<strong><% for(String error : errors){ %>
						<li><%=error %></li>
					<% } %>
  					</strong>
				</div>
				<% } %>
			<div class="col-md-4 col-md-offset-4">
			<h1>物品売上管理システム</h1>

			<input type="mail" name="chargeMail" class="form-control" placeholder="メールアドレス" value="${param.chargeMail}" autofocus>
			<input type="password" name="chargePassword" class="form-control" placeholder="パスワード" value="${param.chargePassword}" autofocus>
			<button type="submit" class="btn btn- btn-primary btn-block">ログイン</button>
			</div>

			</div>
	</div>
</body>
</html>