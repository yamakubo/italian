<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8" import="java.util.*,jp.ssie.pasta.*"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<jsp:useBean id="list" class="java.util.ArrayList"
	type="java.util.ArrayList<Acount>" scope="request" />

<jsp:useBean id="bean" class="jp.ssie.pasta.S0030" scope="request" />


<!DOCTYPE html>
<html lang="ja">
<head>
<meta charset="utf-8">
<meta http-equiv="X-UA-Compatible" content="IE=edge">
<meta name="viewport" content="width=device-width, initial-scale=1">

<title>アカウント登録</title>

<link rel="stylesheet"
	href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css"
	integrity="sha384-BVYiiSIFeK1dGmJRAkycuHAHRg32OmUcww7on3RYdg4Va+PmSTsz/K68vbdEjh4u"
	crossorigin="anonymous">
<link rel="stylesheet"
	href="https://use.fontawesome.com/releases/v5.8.2/css/all.css"
	integrity="sha384-oS3vJWv+0UjzBfQzYUhtDYW+Pj2yciDJxpsK1OYPAYjqT085Qq/1cq5FLXAZQ7Ay"
	crossorigin="anonymous">
<style>
body {
	padding-top: 70px;
}
</style>
</head>
<body>
	<nav class="navbar navbar-default navbar-fixed-top">
		<div class="container">
			<!-- Brand and toggle get grouped for better mobile display -->
			<div class="navbar-header">
				<button type="button" class="navbar-toggle collapsed"
					data-toggle="collapse" data-target="#gnav" aria-expanded="false">
					<span class="sr-only">Toggle navigation</span> <span
						class="icon-bar"></span> <span class="icon-bar"></span> <span
						class="icon-bar"></span>
				</button>
				<a class="navbar-brand" href="C0020.jsp">物品売上管理システム</a>
			</div>

			<div class="collapse navbar-collapse" id="gnav">
				<ul class="nav navbar-nav" mr-auto>
					<li><a href="C0020.jsp">ダッシュボード</a></li>
					<li><a href="S0010.jsp">売上登録</a></li>
					<li><a href="S0020">売上検索</a></li>
					<li class="active"><a href="acount">アカウント登録</a></li>
					<li><a href="S0040.jsp">アカウント検索</a></li>
				</ul>
				<ul class="nav navbar-nav navbar-right">
					<li><a href="C0010.jsp">ログアウト</a></li>
				</ul>

			</div>
		</div>
	</nav>
	<div class="container">
		<form class="form-horizontal" method="POST" action="acount">
			<input type="hidden" name="pg" value="S0030">
			<h1>アカウントを登録してよろしいですか？</h1>
			<!--div class ="row"-->
			<!--div class="col-md-8 col-md-offset-1"-->
			<!--div class="col-md-10 col-md-offset-2"-->
			<div class="form-group">
				<label class="col-md-2 control-label">氏名 <span class="badge">必須</span></label>
				<div class="col-md-7">

					<input type="text" class="form-control"
						value=<%=request.getParameter("chargeName")%> disabled> <input
						type="hidden" class="form-control" name="chargeName" value=<%=request.getParameter("chargeName")%> >
				</div>
			</div>

			<!--div class="col-md-10 col-md-offset-2"-->
			<div class="form-group">
				<label class="col-md-2 control-label">メールアドレス <span
					class="badge">必須</span></label>
				<div class="col-md-7">
					<input type="email" class="form-control"
						value=<%=request.getParameter("chargeMail")%> disabled> <input
						type="hidden" class="form-control" name="chargeMail"
						value=<%=request.getParameter("chargeMail")%>>
				</div>
			</div>

			<!--div class="col-md-10 col-md-offset-2"-->
			<div class="form-group">
				<label class="col-md-2 control-label">パスワード <span
					class="badge">必須</span></label>
				<div class="col-md-7">
					<input type="password" class="form-control"
						value=<%=request.getParameter("chargePassword")%> disabled>
					<input type="hidden" class="form-control" name="chargePassword"
						value=<%=request.getParameter("chargePassword")%>>
				</div>
			</div>
			<!--div class="col-md-10 col-md-offset-2"-->
			<div class="form-group">
				<label class="col-md-2 control-label">パスワード(確認) <span
					class="badge">必須</span></label>
				<div class="col-md-7">
					<input type="password" class="form-control"
						value=<%=request.getParameter("chargePassConfirm")%> disabled>
				</div>
			</div>

			<!--div class="col-md-10 col-md-offset-2"-->
			<div class="form-group">
				<label class="col-md-2 control-label">権限 <span class="badge">必須</span></label>
				<div class="col-md-7">
					<label class="form-control-static">
					<%
 							String s10 = "";
 							String s20 = "";
 							String s30 = "";

 					if (Integer.parseInt(request.getParameter("chargeAuthLevel")) == 10) {
 							s10 = " checked ";
 					}
	 				if (Integer.parseInt(request.getParameter("chargeAuthLevel")) == 20) {
 							s20 = " checked ";
				 	}
 					if (Integer.parseInt(request.getParameter("chargeAuthLevel")) == 30) {
 							s30 = " checked ";
 					}

 					%>



 					<input type="hidden" name="chargeAuthLevel" <%= s10 %> value=<%=request.getParameter("chargeAuthLevel")%>>
					<input type="hidden" name="chargeAuthLevel" <%= s20 %> value=<%=request.getParameter("chargeAuthLevel")%>>
					<input type="hidden" name="chargeAuthLevel" <%= s30 %> value=<%=request.getParameter("chargeAuthLevel")%>>

 					<input type="radio" value=<%=request.getParameter("chargeAuthLevel")%> <%= s10 %> disabled> 権限なし</label>
					<label class="form-control-static">
					<input type="radio" value=<%=request.getParameter("chargeAuthLevel")%> <%= s20 %> disabled> 参照</label>
					<label class="form-control-static">
					<input type="radio" value=<%=request.getParameter("chargeAuthLevel")%> <%= s30 %> disabled> 更新</label>
				</div>
			</div>
			<div class="center-block">
				<div class="col-md-4 col-md-offset-4">
					<button class="btn btn-primary">
						<i class="fas fa-check"></i> OK
					</button>
					<a href="S0030.jsp"><button class="btn btn-default">キャンセル</button></a>
				</div>
			</div>
		</form>
	</div>
</body>
</html>