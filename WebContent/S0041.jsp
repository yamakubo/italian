<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8" import="java.util.*,jp.ssie.pasta.*"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>

<jsp:useBean id="list" class="java.util.ArrayList"
	type="java.util.ArrayList<S0041>" scope="request" />
<jsp:useBean id="bean" class="jp.ssie.pasta.S0041" scope="request" />


<!DOCTYPE html>
<html lang="ja">
<head>
<meta charset="utf-8">
<meta http-equiv="X-UA-Compatible" content="IE=edge">
<meta name="viewport" content="width=device-width, initial-scale=1">
<!-- The above 3 meta tags *must* come first in the head; any other head content must come *after* these tags -->
<title>アカウント検索結果表示</title>

<!-- Bootstrap -->
<link rel="stylesheet"
	href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css"
	integrity="sha384-BVYiiSIFeK1dGmJRAkycuHAHRg32OmUcww7on3RYdg4Va+PmSTsz/K68vbdEjh4u"
	crossorigin="anonymous">
<link rel="stylesheet"
	href="https://use.fontawesome.com/releases/v5.8.2/css/all.css"
	integrity="sha384-oS3vJWv+0UjzBfQzYUhtDYW+Pj2yciDJxpsK1OYPAYjqT085Qq/1cq5FLXAZQ7Ay"
	crossorigin="anonymous">
<!-- HTML5 shim and Respond.js for IE8 support of HTML5 elements and media queries -->
<!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
<!--[if lt IE 9]>
      <script src="https://oss.maxcdn.com/html5shiv/3.7.3/html5shiv.min.js"></script>
      <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
    <![endif]-->
<style>
body {
	padding-top: 70px;
}
</style>
</head>
<body>
	<nav class="navbar navbar-default navbar-fixed-top">
		<div class="container">
			<!-- Brand and toggle get grouped for better mobile display -->
			<div class="navbar-header">
				<button type="button" class="navbar-toggle collapsed"
					data-toggle="collapse" data-target="#gnav" aria-expanded="false">
					<span class="sr-only">Toggle navigation</span> <span
						class="icon-bar"></span> <span class="icon-bar"></span> <span
						class="icon-bar"></span>
				</button>
				<a class="navbar-brand" href="C0020.jsp">物品売上管理システム</a>
			</div>

			<!-- Collect the nav links, forms, and other content for toggling -->
			<div class="collapse navbar-collapse" id="gnav">
				<ul class="nav navbar-nav" mr-auto>
					<li><a href="C0020">ダッシュボード</a></li>
					<li><a href="S0010">売上登録</a></li>
					<li><a href="S0020">売上検索</a></li>
					<li><a href="acount">アカウント登録</a></li>
					<li class="active"><a href="S0040.jsp">アカウント検索</a></li>
				</ul>
				<ul class="nav navbar-nav navbar-right">
					<li><a href="C0010">ログアウト</a></li>
				</ul>

			</div>
			<!-- /.navbar-collapse -->
		</div>
		<!-- /.container-fluid -->
	</nav>
	<div class="container">
		<h1>アカウント検索結果表示</h1>
		<form method="GET" action="acount">
			<input type="hidden" name="pg" value="S0042">
			<div class="row">
				<table class="table">
					<tr>
						<th><div class="text-left">操作</div></th>
						<th>No</th>
						<th>氏名</th>
						<th>メールアドレス</th>
						<th>権限</th>

					</tr>
					<%
						for (S0041 a : list) {
					%>
					<!--<input type="hidden" name="pg"
						value="S0041&charge_id=<%=a.getChargeId()%>" > -->
					<tr>
						<td><a
							href="acount?pg=S0042&charge_id=<%=a.getChargeId()%>"
							class="btn btn-primary" role="submit"><i class="fas fa-check"></i>
								編集</a> <a href="acount?pg=S0044&charge_id=<%=a.getChargeId()%>"
							class="btn btn-danger" role="submit"><i class="fas fa-times"></i>
								削除</a></td>
						<td><%=a.getChargeId()%></td>
						<td><%=a.getChargeName()%></td>
						<td><%=a.getChargeMail()%></td>
						<td><%
 									String s10 = "";
 									String s20 = "";
 									String s30 = "";

 								if (a.getChargeAuthLevel() == 10) {
 										s10 = " 権限なし ";
 								}
	 							if (a.getChargeAuthLevel() == 20) {
 										s20 = " 売上登録 ";
				 				}
 								if (a.getChargeAuthLevel() == 30) {
 										s30 = " アカウント登録 ";
 								}

 								%>

								<input type="hidden" name="chargeAuthLevel" <%= s10 %> value=<%=request.getParameter("chargeAuthLevel")%>>

								<% out.print(s10); %>
								<% out.print(s20); %>
								<% out.print(s30); %>
						</td>
					</tr>

					<%
						}
					%>
				</table>
			</div>
		</form>
	</div>
	<!-- jQuery (necessary for Bootstrap's JavaScript plugins) -->
	<script
		src="https://ajax.googleapis.com/ajax/libs/jquery/1.12.4/jquery.min.js"></script>
	<!-- Include all compiled plugins (below), or include individual files as needed -->
	<script
		src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js"
		integrity="sha384-Tc5IQib027qvyjSMfHjOMaLkfuWVxZxUPnCJA7l2mCWNIpG9mGCD8wGNIcPD7Txa"
		crossorigin="anonymous"></script>

</body>
</html>