<%@page contentType="text/html; charset=UTF-8"
	import="java.util.*,jp.ssie.pasta.*"%>
<jsp:useBean id="bean" class ="jp.ssie.pasta.S0021" scope="request"/>

<!DOCTYPE html>
<html lang="ja">
	<head>
    	<meta charset="utf-8">
    	<meta http-equiv="X-UA-Compatible" content="IE=edge">
    	<meta name="viewport" content="width=device-width, initial-scale=1">
    	<!-- The above 3 meta tags *must* come first in the head; any other head content must come *after* these tags -->
    	<title>売上詳細表示</title>

    	<!-- Bootstrap -->
	    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css" integrity="sha384-BVYiiSIFeK1dGmJRAkycuHAHRg32OmUcww7on3RYdg4Va+PmSTsz/K68vbdEjh4u" crossorigin="anonymous">
		<link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.8.2/css/all.css" integrity="sha384-oS3vJWv+0UjzBfQzYUhtDYW+Pj2yciDJxpsK1OYPAYjqT085Qq/1cq5FLXAZQ7Ay" crossorigin="anonymous">
	    <!-- HTML5 shim and Respond.js for IE8 support of HTML5 elements and media queries -->
	    <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
	    <!--[if lt IE 9]>
	      <script src="https://oss.maxcdn.com/html5shiv/3.7.3/html5shiv.min.js"></script>
	      <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
	    <![endif]-->
	    <style>
	    body { padding-top: 70px; }
	    </style>
	</head>
	<body>
		<nav class="navbar navbar-default navbar-fixed-top" >
  		<div class="container">
    <!-- Brand and toggle get grouped for better mobile display -->
    		<div class="navbar-header">
      			<button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#gnav" aria-expanded="false">
      				<span class="sr-only">Toggle navigation</span>
        			<span class="icon-bar"></span>
        			<span class="icon-bar"></span>
       				<span class="icon-bar"></span>
				</button>
      			<a class="navbar-brand" href="C0020.html">物品売上管理システム</a>
    		</div>

   	 <!-- Collect the nav links, forms, and other content for toggling -->
			<div class="collapse navbar-collapse" id="gnav">
      			<ul class="nav navbar-nav" mr-auto>
      			<li><a href="C0020">ダッシュボード</a></li>
        		<li><a href="S0010">売上登録</a></li>
        		<li class="active"><a href="S0020">売上検索</a></li>
       			<li><a href="S0030">アカウント登録</a></li>
        		<li><a href="S0040">アカウント検索</a></li>
				</ul>
				<ul class="nav navbar-nav navbar-right">
				<li><a href="C0010">ログアウト</a></li>
				</ul>

    		</div><!-- /.navbar-collapse -->
		</div><!-- /.container-fluid -->
	</nav>
	<div class="container">
	<form class="form-horizontal" method="GET" action="S0020">
	<input type="hidden" name="pg" value="S0023">
	<input type="hidden" name="saleId" value="<%=bean.getSaleId() %>">
		<h1>売上詳細表示</h1>
		<!--div class ="row"-->
		<!--div class="col-md-8 col-md-offset-1"-->

				<!--div class="col-md-10 col-md-offset-2"-->
					<div class="form-group">
						<label class="col-md-2 control-label">販売日</label>
						<div class="col-md-7">
							<p class="form-control-static" name="saleDate"><%=bean.getSaleDate() %>

							</p>
						</div>
					</div>

				<!--div class="col-md-10 col-md-offset-2"-->
					<div class ="form-group">
						<label class="col-md-2 control-label">担当</label>
						<div class="col-md-7">
      						<p class="form-control-static" name="chargeId"><%=bean.getChargeName() %></p>
     		 			</div>
					</div>

				<!--div class="col-md-10 col-md-offset-2"-->
					<div class ="form-group">
						<label class="col-md-2 control-label">商品カテゴリー</label>
						<div class="col-md-7">
							<p class="form-control-static" name="categoryId"><%=bean.getCategoryName()%></p>
						</div>
					</div>
					<div class ="form-group">
						<label class="col-md-2 control-label">商品名</label>
						<div class="col-md-7">
							<p class="form-control-static" name="productName"><%=bean.getProductName() %></p>
						</div>
					</div>

				<!--div class="col-md-10 col-md-offset-2"-->
					<div class ="form-group">
						<label class="col-md-2 control-label">単価</label>
						<div class="col-md-7">
							<p class="form-control-static" name="productPrice"><%=bean.getPrice()%></p>
						</div>
					</div>
					<div class ="form-group">
						<label class="col-md-2 control-label">個数</label>
						<div class="col-md-7">
							<p class="form-control-static" name="productCount"><%=bean.getCount()%></p>
						</div>
					</div>
					<div class ="form-group">
						<label class="col-md-2 control-label">備考</label>
						<div class="col-md-7">
							<p class="form-control-static"  name="saleRemarks"><%=bean.getSaleRemarks()%></p>
						</div>
					</div>



			<div class="center-block">
				<div class="col-md-4 col-md-offset-4">
					<button class="btn btn-primary" type="submit"><i class="fas fa-check"></i> 編　集</button>
					<a href="S0020?pg=S0025&saleId=<%=bean.getSaleId() %>" class="btn btn-danger" role="submit"><i class="fas fa-times"></i> 削　除</a>
					<a href="S0020?pg=S0021" class="btn btn-default" role="submit"> キャンセル</a>
				</div>
			</div>
	</div>
		      <!-- jQuery (necessary for Bootstrap's JavaScript plugins) -->
		    <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.12.4/jquery.min.js"></script>
		    <!-- Include all compiled plugins (below), or include individual files as needed -->
		    <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js" integrity="sha384-Tc5IQib027qvyjSMfHjOMaLkfuWVxZxUPnCJA7l2mCWNIpG9mGCD8wGNIcPD7Txa" crossorigin="anonymous"></script>
	</form>
	</body>
</html>
