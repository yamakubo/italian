<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8" import="java.util.*,jp.ssie.pasta.*"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<jsp:useBean id="list" class="java.util.ArrayList"
	type="java.util.ArrayList<Acount>" scope="request" />

<jsp:useBean id="bean" class="jp.ssie.pasta.S0030" scope="request" />

<% List<String> errors = (List<String>)request.getAttribute("errors"); %>


<!DOCTYPE html>
<html lang="ja">
<head>
<meta charset="utf-8">
<meta http-equiv="X-UA-Compatible" content="IE=edge">
<meta name="viewport" content="width=device-width, initial-scale=1">
<title>アカウント登録</title>

<!-- Bootstrap -->
<link rel="stylesheet"
	href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css"
	integrity="sha384-BVYiiSIFeK1dGmJRAkycuHAHRg32OmUcww7on3RYdg4Va+PmSTsz/K68vbdEjh4u"
	crossorigin="anonymous">
<link rel="stylesheet"
	href="https://use.fontawesome.com/releases/v5.8.2/css/all.css"
	integrity="sha384-oS3vJWv+0UjzBfQzYUhtDYW+Pj2yciDJxpsK1OYPAYjqT085Qq/1cq5FLXAZQ7Ay"
	crossorigin="anonymous">

<style>
body {
	padding-top: 70px;
}
</style>
</head>
<body>
	<nav class="navbar navbar-default navbar-fixed-top">
		<div class="container">
			<!-- Brand and toggle get grouped for better mobile display -->
			<div class="navbar-header">
				<button type="button" class="navbar-toggle collapsed"
					data-toggle="collapse" data-target="#gnav" aria-expanded="false">
					<span class="sr-only">Toggle navigation</span> <span
						class="icon-bar"></span> <span class="icon-bar"></span> <span
						class="icon-bar"></span>
				</button>
				<a class="navbar-brand" href="C0020.jsp">物品売上管理システム</a>
			</div>

			<!-- Collect the nav links, forms, and other content for toggling -->
			<div class="collapse navbar-collapse" id="gnav">
				<ul class="nav navbar-nav" mr-auto>
					<li><a href="C0020.jsp">ダッシュボード</a></li>
					<li><a href="S0010.jsp">売上登録</a></li>
					<li><a href="S0020">売上検索</a></li>
					<li class="active"><a href="acount">アカウント登録</a></li>
					<li><a href="S0040.jsp">アカウント検索</a></li>
				</ul>
				<ul class="nav navbar-nav navbar-right">
					<li><a href="C0010.jsp">ログアウト</a></li>
				</ul>

			</div>
			<!-- /.navbar-collapse -->
		</div>
		<!-- /.container-fluid -->
	</nav>
	<div class="container">
		<h1>アカウント登録</h1>
		<form class="form-horizontal" method="POST" action="acount">
			<input type="hidden" name="pg" value="S0031">
				<% if (errors != null && errors.size() != 0) { %>
			<div class="alert alert-danger" role="alert">
				<strong>
					<% for (String error : errors) { %>
						<li><%=error%></li>
					<% } %>
				</strong>
			</div>
				<% } %>
			<div class="form-group">
				<label class="col-md-2 control-label">氏名 <span class="badge">必須</span></label>
				<div class="col-md-7">
					<input type="text" class="form-control" name="chargeName"
						placeholder="氏名" value="${ param.chargeName }">
				</div>
			</div>

			<!--div class="col-md-10 col-md-offset-2"-->
			<div class="form-group">
				<label class="col-md-2 control-label">メールアドレス <span
					class="badge">必須</span></label>
				<div class="col-md-7">
					<input type="email" class="form-control" name="chargeMail"
						placeholder="メールアドレス" value="${ param.chargeMail }">
				</div>
			</div>

			<!--div class="col-md-10 col-md-offset-2" -->
			<div class="form-group">
				<label class="col-md-2 control-label">パスワード <span
					class="badge">必須</span></label>
				<div class="col-md-7">
					<input type="password" class="form-control" name="chargePassword"
						placeholder="パスワード" value="${ param.chargePassword }">
				</div>
			</div>
			<!--div class="col-md-10 col-md-offset-2"-->
			<div class="form-group">
				<label class="col-md-2 control-label">パスワード(確認) <span
					class="badge">必須</span></label>
				<div class="col-md-7">
					<input type="password" class="form-control" name="chargePassConfirm"
						placeholder="パスワード(確認)" value="${ param.chargePassConfirm }">
				</div>
			</div>

			<!--div class="col-md-10 col-md-offset-2"-->
			<div class="form-group">
				<label class="col-md-2 control-label">権限 <span class="badge">必須</span></label>
				<div class="col-md-7">
					<label class="form-control-static"> <input type="radio"
						name="chargeAuthLevel" value="10" checked> 権限なし
					</label> <label class="form-control-static"> <input type="radio"
						name="chargeAuthLevel" value="20"> 参照
					</label> <label class="form-control-static"> <input type="radio"
						name="chargeAuthLevel" value="30"> 更新
					</label>
				</div>
			</div>
			<div class="center-block">
				<div class="col-md-4 col-md-offset-4">
					<button class="btn btn-primary" role="button">
						<i class="fas fa-check"></i> 登 録
					</button>
				</div>
			</div>
		</form>
	</div>
</body>
</html>

