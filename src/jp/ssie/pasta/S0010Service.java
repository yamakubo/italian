package jp.ssie.pasta;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.util.ArrayList;
import java.util.List;

public class S0010Service {
	List<String> validation(Sell sell, Connection con) {
		List<String> errors = new ArrayList<>();

		try {
			if (sell.getSaleDate() == "") {
				errors.add("販売日を入力してください。");
			}
		} catch (Exception e) {

		}
		try {
			if (sell.getSaleDate() != "" && !sell.getSaleDate().matches("\\d{4}\\-\\d{2}\\-\\d{2}")) {
				errors.add("販売日を正しく入力して下さい。");
			}
		} catch (Exception e) {

		}
		if (sell.getChargeId() == 0) {
			errors.add("担当が未選択です");
		}

		if (sell.getCategoryId() == 0) {
			errors.add("商品カテゴリーが未選択です");
		}


			PreparedStatement ps = null;
			ResultSet rs = null;
			try {
				ps = con.prepareStatement("select * from charge where charge_id = ?");
				ps.setInt(1, sell.getChargeId());
				rs = ps.executeQuery();
				if (!rs.next()) {
					errors.add("アカウントテーブルに存在しません");
				}
			} catch (Exception e) {
			} finally {
				try {
					if (ps != null) {
						ps.close();
					}
					if (rs != null) {
						rs.close();
					}
				} catch (Exception e) {
				}
			}
			try {
				ps = con.prepareStatement("select * from category where category_id = ?");
				ps.setInt(1, sell.getCategoryId());
				rs = ps.executeQuery();
				if (!rs.next()) {
					errors.add("商品カテゴリーが存在しません");
				}
			} catch (Exception e) {
			} finally {
				try {
					if (ps != null) {
						ps.close();
					}
					if (rs != null) {
						rs.close();
					}
				} catch (Exception e) {
				}
			}

		if (sell.getProductName().length() == 0) {
			errors.add("商品名を入力して下さい。");
		}
		if (sell.getProductName().length() > 50) {
			errors.add("商品名が長すぎます。");
		}
		if (sell.getProductPrice() == 0) {
			errors.add("単価を入力して下さい");
		}

		if (sell.getProductPrice() > 999999999) {
			errors.add("単価が長すぎます。");
		}
		String price = String.valueOf(sell.getProductPrice());
		if (!price.matches("^[0-9]*$")) {
			errors.add("単価を正しく入力して下さい。");
		}

		if (sell.getProductCount() == 0) {
			errors.add("個数を入力して下さい。");
		}

		if (sell.getProductCount() > 999999999) {
			errors.add("個数が長すぎます。");
		}
		String count = String.valueOf(sell.getProductCount());
		if (!count.matches("^[0-9]*$")) {
			errors.add("個数を正しく入力して下さい。");
		}
		if (sell.getSaleRemarks().length() > 200) {
			errors.add("備考が長すぎます。");
		}
		return errors;

	}

	void regist(Connection con, Sell s) {
		PreparedStatement ps = null;
		java.sql.Date sqlDate = java.sql.Date.valueOf(s.getSaleDate());
		try {
			ps = con.prepareStatement(
					"insert into sale (sale_date, charge_id, category_id, product_name, product_price, product_count, sale_remarks, sale_delete_flg) values (?, ?, ?, ?, ?, ?, ?, ?)");
			ps.setDate(1, sqlDate);
			ps.setInt(2, s.getChargeId());
			ps.setInt(3, s.getCategoryId());
			ps.setString(4, s.getProductName());
			ps.setInt(5, s.getProductPrice());
			ps.setInt(6, s.getProductCount());
			ps.setString(7, s.getSaleRemarks());
			ps.setInt(8, 0);
			ps.executeUpdate();
		} catch (Exception ee) {
			ee.printStackTrace();
		} finally {
			try {
				if (ps != null)
					ps.close();
			} catch (Exception ee) {
			}
		}
	}

	List<String[]> getCharges(Connection con) {
		List<String[]> res = new ArrayList<>();
		PreparedStatement ps = null;
		ResultSet rs = null;
		try {

			ps = con.prepareStatement("select * from charge order by charge_name");
			rs = ps.executeQuery();
			while (rs.next()) {

				res.add(new String[] { rs.getString("charge_id"), rs.getString("charge_name") });
			}
		} catch (Exception e) {
			e.printStackTrace();
		} finally {
			try {
				if (ps != null)
					ps.close();
			} catch (Exception ee) {
			}
		}
		return res;

	}

	List<String[]> getCategories(Connection con) {
		List<String[]> res = new ArrayList<>();
		PreparedStatement ps = null;
		ResultSet rs = null;
		try {

			ps = con.prepareStatement("select * from category order by category_name");
			rs = ps.executeQuery();
			while (rs.next()) {

				res.add(new String[] { rs.getString("category_id"), rs.getString("category_name") });
			}
		} catch (Exception e) {
			e.printStackTrace();
		} finally {
			try {
				if (ps != null)
					ps.close();
			} catch (Exception ee) {
			}
		}
		return res;

	}

}
