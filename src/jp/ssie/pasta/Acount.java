package jp.ssie.pasta;

import java.io.Serializable;

import lombok.Data;

@Data

public class Acount implements Serializable {

	private int chargeId;
	private String chargeName;
	private String chargeMail;
	private String chargePassword;
	private String chargePassConfirm;
	private int chargeAuthLevel;
	private int chargeDeleteFlg;


}
