package jp.ssie.pasta;

import java.io.IOException;
import java.net.URLEncoder;
import java.sql.Connection;
import java.util.ArrayList;
import java.util.List;

import javax.naming.InitialContext;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import javax.sql.DataSource;

/**
 * Servlet implementation class S0020Controller
 */
@WebServlet("/S0020")
public class S0020Controller extends HttpServlet {
	private static final long serialVersionUID = 1L;

	protected void doGet(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {
		// TODO Auto-generated method stub
		//ログイン情報がないとき、ログイン画面に強制送還。
		HttpSession session = request.getSession();
		List<String> lu = (List<String>) session.getAttribute("login");
		if (lu == null) {
			response.sendRedirect("C0010");
			return;
		}

		S0020Service service = new S0020Service();
		Connection con = getConnection();
		String pg = request.getParameter("pg");
		String jsp = "/S0020.jsp";
		request.setAttribute("charges", service.getCharges(con));
		request.setAttribute("categories", service.getCategories(con));

		if (pg != null && pg.equals("S0020")) {
			jsp = "/S0020.jsp";
			request.setAttribute("charges", service.getCharges(con));
			request.setAttribute("categories", service.getCategories(con));

		}
		if (pg != null && pg.equals("S0021")) {
			List<String> searches = new ArrayList<String>();

			searches.add(request.getParameter("saleDateFrom"));
			searches.add(request.getParameter("saleDateTo"));
			searches.add(request.getParameter("chargeId"));
			searches.add(request.getParameter("categoryId"));
			searches.add(request.getParameter("productName"));
			searches.add(request.getParameter("saleRemarks"));
			session.setAttribute("searches", searches);
			request.setAttribute("charges", service.getCharges(con));
			request.setAttribute("categories", service.getCategories(con));
			Sell s = new Sell();
			S0020search se = new S0020search();



			/*LocalDate from = null;
			LocalDate to = null;
			try {
				from = LocalDate.parse(request.getParameter("saleDateFrom"));
				to = LocalDate.parse(request.getParameter("saleDateTo"));
			} catch (Exception e) {
			}*/

			se.setFrom(request.getParameter("from"));
			se.setTo(request.getParameter("to"));
			try {
				se.setChargeId(Integer.parseInt(request.getParameter("chargeId")));
			} catch (NumberFormatException e) {

			}
			try {
				se.setCategoryId(Integer.parseInt(request.getParameter("categoryId")));
			} catch (NumberFormatException e) {

			}
			se.setProductName((request.getParameter("productName")));
			se.setSaleRemarks((request.getParameter("saleRemarks")));
			jsp = "/S0021.jsp";
			request.setAttribute("list", service.getList(con, se));
			List<String> dataErrors = service.saleSearchDataValidation(con, se, s);
			if (dataErrors.size() != 0) {
				request.setAttribute("dataErrors", dataErrors);

				pg = "S0020";
				jsp = "/S0020.jsp";

			} else {
				jsp = "/S0021.jsp";

			}

		}
		//詳細画面
		if (pg != null && pg.equals("S0022")) {

			if (session == null) {
				jsp = "/C0010.jsp";
			}
			jsp = "/S0022.jsp";
			Sell sell = new Sell();
			request.setAttribute("charges", service.getCharges(con));
			request.setAttribute("categories", service.getCategories(con));
			String saleId = request.getParameter("saleId");
			sell.setSaleId(Integer.parseInt(saleId));
			request.setAttribute("bean", service.getDetail(con, sell));

		}
		//詳細編集
		if (pg != null && pg.equals("S0023")) {

			if (session == null) {
				jsp = "/C0010.jsp";
			}
			jsp = "/S0023.jsp";
			Sell sell = new Sell();
			request.setAttribute("charges", service.getCharges(con));
			request.setAttribute("categories", service.getCategories(con));
			String saleId = request.getParameter("saleId");
			sell.setSaleId(Integer.parseInt(saleId));
			request.setAttribute("bean", service.getDetail(con, sell));

		}

		//削除確認
		if (pg != null && pg.equals("S0025")) {

			if (session == null) {
				jsp = "/C0010.jsp";
			}
			jsp = "/S0025.jsp";
			Sell sell = new Sell();
			request.setAttribute("charges", service.getCharges(con));
			request.setAttribute("categories", service.getCategories(con));
			String saleId = request.getParameter("saleId");
			sell.setSaleId(Integer.parseInt(saleId));
			request.setAttribute("bean", service.getDetail(con, sell));

		}

		try {
			con.close();
		} catch (Exception e) {
			// TODO 自動生成された catch ブロック
			e.printStackTrace();
		}
		getServletContext().getRequestDispatcher(jsp).forward(request, response);


	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {
		request.setCharacterEncoding("UTF-8");
		String pg = request.getParameter("pg");
		String jsp = "/C0010.jsp";
		S0020Service service = new S0020Service();
		Connection con = getConnection();
		request.setAttribute("charges", service.getCharges(con));
		request.setAttribute("categories", service.getCategories(con));

		if (pg != null && pg.equals("S0021")) {
			HttpSession session = request.getSession();
			if (session == null) {
				jsp = "/C0010.jsp";
			}
			jsp = "/S0021.jsp";
			String deleteId = request.getParameter("deleteId");
			if (deleteId != null) {
				Sell se = new Sell();
				se.setSaleId(Integer.parseInt(deleteId));
				service.delete(con, se);
			}

			String updateId = request.getParameter("updateId");
			if (updateId != null) {
				Sell sl = new Sell();

				sl.setSaleDate(request.getParameter("saleDate"));
				sl.setChargeId(Integer.parseInt(request.getParameter("chargeId")));
				sl.setCategoryId(Integer.parseInt(request.getParameter("categoryId")));
				sl.setProductName(request.getParameter("productName"));
				sl.setProductPrice(Integer.parseInt(request.getParameter("productPrice")));
				sl.setProductCount(Integer.parseInt(request.getParameter("productCount")));
				sl.setSaleRemarks(request.getParameter("saleRemarks"));
				sl.setSaleId(Integer.parseInt(updateId));
				service.update(con, sl);
			}
			List<String> searches = (List<String>) session.getAttribute("searches");
			String dateFrom = searches.get(0);
			String dateTo = searches.get(1);
			String chargeId = searches.get(2);
			String categoryId = searches.get(3);
			String productName = URLEncoder.encode(searches.get(4), "UTF-8");
			String saleRemarks = URLEncoder.encode(searches.get(5), "UTF-8");

			response.sendRedirect(
					"S0020?pg=S0021&saleDate=" + dateFrom + "&saleDate=" + dateTo + "&chargeId=" + chargeId
							+ "&categoryId=" + categoryId + "&productName=" + productName + "&saleRemarks="
							+ saleRemarks);
			return;
		}

		//詳細編集確認
		if (pg != null && pg.equals("S0024")) {
			HttpSession session = request.getSession();
			if (session == null) {
				jsp = "/C0010.jsp";
			}
			Sell se = new Sell();
			try {
				se.setSaleDate(request.getParameter("saleDate"));
				se.setChargeId(Integer.parseInt(request.getParameter("chargeId")));

			} catch (Exception e) {

			}
			try {
				se.setCategoryId(Integer.parseInt(request.getParameter("categoryId")));
			} catch (Exception e) {

			}
			se.setProductName(request.getParameter("productName"));
			try {
				se.setProductPrice(Integer.parseInt(request.getParameter("productPrice")));
			} catch (Exception e) {

			}
			try {
				se.setProductCount(Integer.parseInt(request.getParameter("productCount")));
			} catch (Exception e) {

			}

			se.setSaleRemarks(request.getParameter("saleRemarks"));
			List<String> errors = service.validation(se, con);
			if (errors.size() != 0) {
				request.setAttribute("errors", errors);
				pg = "S0023";
				jsp = "/S0023.jsp";
				Sell sell = new Sell();
				String saleId = request.getParameter("saleId");
				sell.setSaleId(Integer.parseInt(saleId));
				request.setAttribute("bean", service.getDetail(con, sell));
				getServletContext().getRequestDispatcher(jsp).forward(request, response);
				return;
			} else {
				jsp = "/S0024.jsp";
			}
			Sell sell = new Sell();
			request.setAttribute("charges", service.getCharges(con));
			request.setAttribute("categories", service.getCategories(con));
			String saleId = request.getParameter("saleId");
			sell.setSaleId(Integer.parseInt(saleId));
			request.setAttribute("bean", service.getDetail(con, sell));
			jsp = "/S0024.jsp";
			getServletContext().getRequestDispatcher(jsp).forward(request, response);
			return;
		}

		try {
			con.close();
		} catch (Exception e) {
			// TODO 自動生成された catch ブロック
			e.printStackTrace();
		}
		doGet(request, response);

	}

	private Connection getConnection() throws ServletException {
		Connection con = null;
		try {
			DataSource ds = (DataSource) new InitialContext().lookup("java:/comp/env/jdbc/pasta");
			con = ds.getConnection();
		} catch (Exception e) {
			throw new ServletException(e);
		}
		return con;
	}

}
