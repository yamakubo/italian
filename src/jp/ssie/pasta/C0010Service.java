package jp.ssie.pasta;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.util.ArrayList;
import java.util.List;

public class C0010Service {
	List<String> loginValidation(C0010 log, Connection con) {
		List<String> loginErrors = new ArrayList<>();

		if (log.getChargeMail().matches("")) {
			loginErrors.add("メールアドレスを入力して下さい。");
		}
		try {
			if (log.getChargeMail().length() > 50) {
				loginErrors.add("メールアドレスが長すぎます。");
			}
		} catch (Exception e) {

		}

		if (!log.getChargeMail().matches("[-_.0-9A-Za-z]+@[-_0-9A-Za-z]+[-_.0-9A-Za-z]+")
				&& log.getChargeMail() != "") {
			loginErrors.add("メールアドレスを正しく入力して下さい。");
		}
		PreparedStatement ps = null;
		ResultSet rs = null;
		try {
			ps = con.prepareStatement("select * from charge where charge_mail = ?");
			ps.setString(1, log.getChargeMail());
			rs = ps.executeQuery();
			if (!rs.next()) {
				loginErrors.add("メールアドレスが存在しません。");
			}
		} catch (Exception e) {
		} finally {
			try {
				if (ps != null) {
					ps.close();
				}
				if (rs != null) {
					rs.close();
				}
			} catch (Exception e) {
			}
		}
		if (log.getChargePassword().matches("")) {
			loginErrors.add("パスワードを入れてください。");
		}

		try {
			if (log.getChargePassword().length() > 15) {
				loginErrors.add("パスワードが長すぎます。");
			}
		} catch (Exception e) {

		}

		PreparedStatement pr = null;
		ResultSet re = null;
		try {
			ps = con.prepareStatement("select * from charge where charge_password = ?");
			ps.setString(1, log.getChargePassword());
			rs = ps.executeQuery();
			if (!rs.next()) {
				loginErrors.add("パスワードが存在しません。");
			}
		} catch (Exception e) {
		} finally {
			try {
				if (ps != null) {
					ps.close();
				}
				if (rs != null) {
					rs.close();
				}
			} catch (Exception e) {
			}
		}

		PreparedStatement pt = null;
		ResultSet rt = null;
		try {
			ps = con.prepareStatement("select * from charge where charge_mail = ? && charge_password = ?");
			ps.setString(1, log.getChargeMail());
			ps.setString(2, log.getChargePassword());
			rs = ps.executeQuery();
			if(!rs.next()) {
				loginErrors.add("メールアドレスとパスワードが一致しません。");
			}
		} catch (Exception e) {
		} finally {
			try {
				if (ps != null) {
					ps.close();
				}
				if (rs != null) {
					rs.close();
				}
			} catch (Exception e) {
			}
		}
		return loginErrors;

	}
}
