package jp.ssie.pasta;

import java.time.LocalDate;

import lombok.Data;

@Data
public class S0021 {
	private int saleId;
	private LocalDate saleDate;
	private int chargeId;
	private String chargeName;
	private int categoryId;
	private String categoryName;
	private String productName;
	private int price;
	private int count;
	private int amount;
	private String saleRemarks;

}
