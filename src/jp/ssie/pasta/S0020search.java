package jp.ssie.pasta;

public class S0020search {
	private String from;
	private String to;
	private int chargeId;
	private int categoryId;
	private String productName;
	private String saleRemarks;
	public String getFrom() {
		return from;
	}
	public void setFrom(String from) {
		this.from = from;
	}
	public String getTo() {
		return to;
	}
	public void setTo(String to) {
		this.to = to;
	}
	public int getChargeId() {
		return chargeId;
	}
	public void setChargeId(int chargeId) {
		this.chargeId = chargeId;
	}
	public int getCategoryId() {
		return categoryId;
	}
	public void setCategoryId(int categoryId) {
		this.categoryId = categoryId;
	}
	public String getProductName() {
		return productName;
	}
	public void setProductName(String productName) {
		this.productName = productName;
	}
	public String getSaleRemarks() {
		return saleRemarks;
	}
	public void setSaleRemarks(String saleRemarks) {
		this.saleRemarks = saleRemarks;
	}



}
