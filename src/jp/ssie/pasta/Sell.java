package jp.ssie.pasta;
import java.io.Serializable;

public class Sell implements Serializable{
	private int saleId;
	private String saleDate;
	private int chargeId;
	private String chargeName;
	private int categoryId;
	private String categoryName;
	private String productName;
	private int productPrice;
	private int productCount;
	private String saleRemarks;
	private int saleDeleteFlg;
	public int getSaleId() {
		return saleId;
	}
	public void setSaleId(int saleId) {
		this.saleId = saleId;
	}
	public String getSaleDate() {
		return saleDate;
	}
	public void setSaleDate(String saleDate) {
		this.saleDate = saleDate;
	}
	public int getChargeId() {
		return chargeId;
	}
	public void setChargeId(int chargeId) {
		this.chargeId = chargeId;
	}
	public String getChargeName() {
		return chargeName;
	}
	public void setChargeName(String chargeName) {
		this.chargeName = chargeName;
	}
	public int getCategoryId() {
		return categoryId;
	}
	public void setCategoryId(int categoryId) {
		this.categoryId = categoryId;
	}
	public String getCategoryName() {
		return categoryName;
	}
	public void setCategoryName(String categoryName) {
		this.categoryName = categoryName;
	}
	public String getProductName() {
		return productName;
	}
	public void setProductName(String productName) {
		this.productName = productName;
	}
	public int getProductPrice() {
		return productPrice;
	}
	public void setProductPrice(int productPrice) {
		this.productPrice = productPrice;
	}
	public int getProductCount() {
		return productCount;
	}
	public void setProductCount(int productCount) {
		this.productCount = productCount;
	}
	public String getSaleRemarks() {
		return saleRemarks;
	}
	public void setSaleRemarks(String saleRemarks) {
		this.saleRemarks = saleRemarks;
	}
	public int getSaleDeleteFlg() {
		return saleDeleteFlg;
	}
	public void setSaleDeleteFlg(int saleDeleteFlg) {
		this.saleDeleteFlg = saleDeleteFlg;
	}

}
