package jp.ssie.pasta;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.util.ArrayList;
import java.util.List;

public class AcountService {
	List<S0041> getList(Connection con, S0040serach se) {
		List<S0041> result = new ArrayList<>();

		PreparedStatement ps = null;
		ResultSet rs = null;
		try {
			int idx = 1;
			StringBuilder sql = new StringBuilder();
			sql.append("select ");
			sql.append("    charge_id,   ");
			sql.append("	charge_name, ");
			sql.append("	charge_mail, ");
			sql.append("	charge_auth_level ");
			sql.append(" from charge  ");
			sql.append("where 1 = 1 ");

			if (se.getChargeName().length() != 0) {
				sql.append("and charge_name like ? ");
			}
			if (se.getChargeMail().length() != 0) {
				sql.append("and charge_mail = ? ");
			}
			if (se.getChargeAuthLevel() != 0) {
				sql.append("and charge_auth_level = ?");
			}
			ps = con.prepareStatement(sql.toString());
			if (se.getChargeName().length() != 0) {
				ps.setString(idx++, "%" + se.getChargeName() + "%");
			}
			if (se.getChargeMail().length() != 0) {
				ps.setString(idx++, se.getChargeMail());
			}
			if (se.getChargeAuthLevel() != 0) {
				ps.setInt(idx++, se.getChargeAuthLevel());
			}
			rs = ps.executeQuery();
			//ps = con.prepareStatement("select * from sale where sale_date and charge_id and category_id and (product_name like ?) and (set_remarks like ?) order by sale_id");
			//rs = ps.executeQuery();
			while (rs.next()) {
				S0041 s = new S0041();
				s.setChargeId(rs.getInt("charge_id"));
				s.setChargeName(rs.getString("charge_name"));
				s.setChargeMail(rs.getString("charge_mail"));
				s.setChargeAuthLevel(rs.getInt("charge_auth_level"));
				result.add(s);
			}
		} catch (Exception e) {
			e.printStackTrace();
		} finally {
			try {
				if (ps != null)
					ps.close();
			} catch (Exception ee) {
			}
		}
		return result;
	}

	void regist(Connection con, Acount a) {
		PreparedStatement ps = null;
		try {
			ps = con.prepareStatement(
					"insert into charge (charge_name, charge_mail, charge_password, charge_auth_level, charge_delete_flg) values (?, ?, ?, ?, ?)");
			ps.setString(1, a.getChargeName());
			ps.setString(2, a.getChargeMail());
			ps.setString(3, a.getChargePassword());
			ps.setInt(4, a.getChargeAuthLevel());
			ps.setInt(5, 0);
			ps.executeUpdate();
		} catch (Exception ee) {
			ee.printStackTrace();
		} finally {
			try {
				if (ps != null)
					ps.close();
			} catch (Exception ee) {
			}
		}
	}

	List<String> validation(Acount acount, Connection con) {
		List<String> errors = new ArrayList<>();

		try {
			if (acount.getChargeName().length() == 0) {
				errors.add("氏名を入力してください。");
			}
		} catch (Exception e) {
		}
		if (acount.getChargeName().length() > 20) {
			errors.add("氏名が長すぎます。");
			System.out.println(acount.getChargeName());
		}

		try {
			if (acount.getChargeMail().length() == 0) {
				errors.add("メールアドレスを入力してください。");
			}
		} catch (Exception e) {

		}

		try {
			if (acount.getChargeMail().length() > 100) {
				errors.add("メールアドレスが長すぎます。");
			}
		} catch (Exception e) {

		}
		if (!acount.getChargeMail().matches(
				"^[b-zA-Z0-9!#$%&'_`/=~\\*\\+\\-\\?\\^\\{\\|\\}]+@[a-zA-Z0-9]+$")) {
			errors.add("メールアドレスを正しく入力してください。");
		}

		try {
			if (acount.getChargePassword().length() == 0) {
				errors.add("パスワードを入力してください。");
			}
		} catch (Exception e) {

		}

		try {
			if (acount.getChargePassword().length() > 30) {
				errors.add("パスワードが長すぎます。");
			}
		} catch (Exception e) {

		}

		try {
			if (acount.getChargePassConfirm().length() == 0) {
				errors.add("パスワード(確認）を入力してください。");
			}
		} catch (Exception e) {

		}

		if (acount.getChargePassConfirm().length() != 0
				&& !acount.getChargePassConfirm().equals(acount.getChargePassword())) {
			errors.add("パスワードとパスワード（確認）の入力内容が異なっています。");
		}
		return errors;

	}

	List<String> updatevalidation(Acount acount) {
		List<String> errors = new ArrayList<>();

		try {
			if (acount.getChargeName().length() == 0) {
				errors.add("氏名を入力してください。");
			}
		} catch (Exception e) {
		}
		if (acount.getChargeName().length() > 20) {
			errors.add("氏名が長すぎます。");
			System.out.println(acount.getChargeName());
		}

		try {
			if (acount.getChargeMail().length() == 0) {
				errors.add("メールアドレスを入力してください。");
			}
		} catch (Exception e) {

		}

		try {
			if (acount.getChargeMail().length() > 100) {
				errors.add("メールアドレスが長すぎます。");
			}
		} catch (Exception e) {

		}

		/*		if (!acount.getChargeMail().matches(
						"^[a-zA-Z0-9!#$%&'_`/=~\\*\\+\\-\\?\\^\\{\\|\\}]+(\\.[a-zA-Z0-9!#$%&'_`/=~\\*\\+\\-\\?\\^\\{\\|\\}]+)*+(.*)@[a-zA-Z0-9][a-zA-Z0-9\\-]*(\\.[a-zA-Z0-9\\-]+)+$")) {
					errors.add("メールアドレスを正しく入力してください。");
				} */
		try {
			if (acount.getChargePassword().length() == 0) {
				errors.add("パスワードを入力してください。");
			}
		} catch (Exception e) {

		}

		try {
			if (acount.getChargePassword().length() > 30) {
				errors.add("パスワードが長すぎます。");
			}
		} catch (Exception e) {

		}

		try {
			if (acount.getChargePassConfirm().length() == 0) {
				errors.add("パスワード(確認）を入力してください。");
			}
		} catch (Exception e) {

		}

		if (acount.getChargePassConfirm().length() != 0
				&& !acount.getChargePassConfirm().equals(acount.getChargePassword())) {
			errors.add("パスワードとパスワード（確認）の入力内容が異なっています。");
		}
		return errors;

	}

	List<String> searchvalidation(Acount acount) {
		List<String> errors = new ArrayList<>();

		if (acount.getChargeName().length() > 20) {
			errors.add("氏名の指定が長すぎます。");
			System.out.println(acount.getChargeName());
		}

		try {
			if (acount.getChargeMail().length() > 100) {
				errors.add("メールアドレスの指定が長すぎます。");
			}
		} catch (Exception e) {

		}

		return errors;

	}

	void delete(Connection con, Acount a) {
		PreparedStatement ps = null;
		try {
			ps = con.prepareStatement("delete from charge where charge_id = ?");
			ps.setInt(1, a.getChargeId());
			ps.executeUpdate();
		} catch (Exception ee) {
			ee.printStackTrace();
		} finally {
			try {
				if (ps != null)
					ps.close();
			} catch (Exception ee) {
			}
		}
	}

	void update(Connection con, Acount a) {
		PreparedStatement ps = null;

		try {
			ps = con.prepareStatement(
					"update charge set charge_name = ?, charge_mail = ?, charge_password = ?, charge_auth_level = ? where charge_id = ?");
			ps.setString(1, a.getChargeName());
			ps.setString(2, a.getChargeMail());
			ps.setString(3, a.getChargePassword());
			ps.setInt(4, a.getChargeAuthLevel());
			ps.setInt(5, a.getChargeId());

			System.out.println(ps);

			ps.executeUpdate();
		} catch (Exception ee) {
			ee.printStackTrace();
		} finally {
			try {
				if (ps != null)
					ps.close();
			} catch (Exception ee) {
			}
		}
		return;
	}

	S0041 getDetail(Connection con, Acount acount) {
		S0041 detail = new S0041();

		PreparedStatement ps = null;
		ResultSet rs = null;
		try {
			int idx = 1;
			StringBuilder sql = new StringBuilder();
			sql.append("select ");
			sql.append("	charge_id,   ");
			sql.append("	charge_name, ");
			sql.append("	charge_mail, ");
			sql.append("	charge_password, ");
			sql.append("	charge_auth_level ");
			sql.append(" from charge ");
			sql.append("where charge_id = ? ");

			ps = con.prepareStatement(sql.toString());
			ps.setInt(1, acount.getChargeId());
			rs = ps.executeQuery();
			if (rs.next()) {

				detail.setChargeId(rs.getInt("charge_id"));
				detail.setChargeName(rs.getString("charge_name"));
				detail.setChargeMail(rs.getString("charge_mail"));
				detail.setChargePassword(rs.getString("charge_password"));
				detail.setChargeAuthLevel(rs.getInt("charge_auth_level"));

			}
		} catch (Exception e) {
			e.printStackTrace();
		} finally {
			try {
				if (ps != null)
					ps.close();
			} catch (Exception ee) {
			}
		}
		return detail;
	}

	List<String[]> getCharges(Connection con) {
		List<String[]> res = new ArrayList<>();
		PreparedStatement ps = null;
		ResultSet rs = null;
		try {

			ps = con.prepareStatement("select * from charge order by charge_name");
			rs = ps.executeQuery();
			while (rs.next()) {

				res.add(new String[] { rs.getString("charge_id"), rs.getString("charge_name") });
			}
		} catch (Exception e) {
			e.printStackTrace();
		} finally {
			try {
				if (ps != null)
					ps.close();
			} catch (Exception ee) {
			}
		}
		return res;

	}

	List<String[]> getCategories(Connection con) {
		List<String[]> res = new ArrayList<>();
		PreparedStatement ps = null;
		ResultSet rs = null;
		try {

			ps = con.prepareStatement("select * from category order by category_name");
			rs = ps.executeQuery();
			while (rs.next()) {

				res.add(new String[] { rs.getString("category_id"), rs.getString("category_name") });
			}
		} catch (Exception e) {
			e.printStackTrace();
		} finally {
			try {
				if (ps != null)
					ps.close();
			} catch (Exception ee) {
			}
		}
		return res;

	}

}
