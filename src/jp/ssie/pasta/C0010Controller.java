package jp.ssie.pasta;

import java.io.IOException;
import java.sql.Connection;
import java.util.ArrayList;
import java.util.List;

import javax.naming.InitialContext;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import javax.sql.DataSource;

/**
 * Servlet implementation class LoginController
 */
@WebServlet("/C0010")
public class C0010Controller extends HttpServlet {
	private static final long serialVersionUID = 1L;


	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub


	    HttpSession session = request.getSession(true);
	    session.invalidate();

	    request.getRequestDispatcher("/C0010.jsp").forward(request, response);
	  }




	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub
		request.setCharacterEncoding("UTF-8");
		HttpSession session = request.getSession();
		List<String> lu = (List<String>)session.getAttribute("login");
		if(lu == null) {
			lu = new ArrayList<String>();
			}
		C0010 log = new C0010();
		Connection con = getConnection();
		String pg = request.getParameter("pg");
		String jsp = "/C0010.jsp";
		C0010Service service = new C0010Service();
		log.setChargeMail(request.getParameter("chargeMail"));
		log.setChargePassword(request.getParameter("chargePassword"));
		List<String> loginErrors = service.loginValidation(log, con);
		if(loginErrors.size() != 0) {
			request.setAttribute("loginErrors", loginErrors);

			pg = "C0010";
			jsp = "/C0010.jsp";
			getServletContext().getRequestDispatcher(jsp).forward(request, response);
			return;
		}else {
			jsp="/C0020.jsp";
			session.setAttribute("login", lu);
		}



		request.getRequestDispatcher(jsp).forward(request, response);
	}
	private Connection getConnection() throws ServletException {
		Connection con = null;
		try {
			DataSource ds = (DataSource) new InitialContext().lookup("java:/comp/env/jdbc/pasta");
			con = ds.getConnection();
		} catch (Exception e) {
			throw new ServletException(e);
		}
		return con;
	}

}
