package jp.ssie.pasta;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.util.ArrayList;
import java.util.List;

public class S0020Service {
	//売上登録、売上編集のエラーチェック
	List<String> validation(Sell sell, Connection con) {
		List<String> errors = new ArrayList<>();

		try {
			if (sell.getSaleDate() == "") {
				errors.add("販売日を入力してください。");
			}
		} catch (Exception e) {

		}
		try {
			if (sell.getSaleDate() == "" && !sell.getSaleDate().matches("\\d{4}\\-\\d{2}\\-\\d{2}")) {
				errors.add("販売日を正しく入力して下さい。");
			}
			}catch(Exception e) {

			}
		if (sell.getChargeId() == 0) {
			errors.add("担当が未選択です");
		}

		if (sell.getCategoryId() == 0) {
			errors.add("商品カテゴリーが未選択です");
		}
		PreparedStatement ps = null;
		ResultSet rs = null;
		try {
			ps = con.prepareStatement("select * from charge where charge_id = ?");
			ps.setInt(1, sell.getChargeId());
			rs = ps.executeQuery();
			if (!rs.next()) {
				errors.add("アカウントテーブルに存在しません");
			}
		} catch (Exception e) {
		} finally {
			try {
				if (ps != null) {
					ps.close();
				}
				if (rs != null) {
					rs.close();
				}
			} catch (Exception e) {
			}
		}
		try {
			ps = con.prepareStatement("select * from category where category_id = ?");
			ps.setInt(1, sell.getCategoryId());
			rs = ps.executeQuery();
			if (!rs.next()) {
				errors.add("商品カテゴリーが存在しません");
			}
		} catch (Exception e) {
		} finally {
			try {
				if (ps != null) {
					ps.close();
				}
				if (rs != null) {
					rs.close();
				}
			} catch (Exception e) {
			}
		}
		if (sell.getProductName().length() == 0) {
			errors.add("商品名を入力して下さい。");
		}
		if (sell.getProductName().length() > 50) {
			errors.add("商品名が長すぎます。");
		}
		if (sell.getProductPrice() == 0) {
			errors.add("単価を入力して下さい");
		}

		if (sell.getProductPrice() > 999999999) {
			errors.add("単価が長すぎます。");
		}
		String price = String.valueOf(sell.getProductPrice());
		if (!price.matches("^[0-9]*$")) {
			errors.add("単価を正しく入力して下さい。");
		}

		if (sell.getProductCount() == 0) {
			errors.add("個数を入力して下さい。");
		}

		if (sell.getProductCount() > 999999999) {
			errors.add("個数が長すぎます。");
		}
		String count = String.valueOf(sell.getProductCount());
		if (!count.matches("^[0-9]*$")) {
			errors.add("個数を正しく入力して下さい。");
		}
		if (sell.getSaleRemarks().length() > 200) {
			errors.add("備考が長すぎます。");
		}
		return errors;

	}



	/*List<String> searchValidation(S0020search search) {
		List<String> errors = new ArrayList<>();
		SimpleDateFormat format = new SimpleDateFormat("yyyy/MM/dd");

			if (search.getFrom().format(DateTimeFormatter.ISO_DATE).length() > 20) {
				errors.add("販売日(検索開始日)を正しく入力して下さい。");
			}
			if (search.getTo().format(DateTimeFormatter.ISO_DATE).length() > 20) {
				errors.add("販売日(検索終了日)を正しく入力して下さい。");
			}

			return errors;

	}*/

	List<String> saleSearchDataValidation(Connection con, S0020search se, Sell sell) {
		List<String> dataErrors = new ArrayList<>();
		try {
			if (se.getFrom().matches("\\d{4}\\-\\d{2}\\-\\d{2}")) {
				dataErrors.add("販売日(検索開始日）を正しく入力して下さい。");
			}
		} catch (Exception e) {

		}
		try {
			if (se.getTo().matches("\\d{4}\\-\\d{2}\\-\\d{2}")) {
				dataErrors.add("販売日(検索終了日）を正しく入力して下さい。");
			}
		} catch (Exception e) {

		}

		PreparedStatement ps = null;
		ResultSet rs = null;
		try {
			int idx = 1;
			StringBuilder sql = new StringBuilder();
			sql.append("select ");
			sql.append("	s.sale_id,  ");
			sql.append("	s.sale_date, ");
			sql.append("	c.charge_id, ");
			sql.append("	c.charge_name, ");
			sql.append("	cat.category_id, ");
			sql.append("	cat.category_name, ");
			sql.append("	s.product_name, ");
			sql.append("	s.product_price, ");
			sql.append("	s.product_count ");
			sql.append(" from sale s ");
			sql.append(" join charge c on( ");
			sql.append("	s.charge_id = c.charge_id ");
			sql.append(") ");
			sql.append(" join category cat on( ");
			sql.append("	s.category_id = cat.category_id ");
			sql.append(") ");
			sql.append("where 1 = 1 ");
			if (se.getFrom() != null && se.getTo() != null) {
				sql.append("and s.sale_date between ? and ? ");
			}
			if (se.getChargeId() != 0) {
				sql.append("and s.charge_id = ? ");
			}
			if (se.getCategoryId() != 0) {
				sql.append("and s.category_id = ? ");
			}
			if (se.getProductName().length() != 0) {
				sql.append("and s.product_name = ?");
			}
			if (se.getSaleRemarks().length() != 0) {
				sql.append("and s.sale_remarks = ? ");
			}
			ps = con.prepareStatement(sql.toString());
			if (se.getFrom() != null & se.getTo() != null) {
				ps.setDate(idx++, java.sql.Date.valueOf(se.getFrom()));
				ps.setDate(idx++, java.sql.Date.valueOf(se.getTo()));
			}
			if (se.getChargeId() != 0) {
				ps.setInt(idx++, se.getChargeId());
			}
			if (se.getCategoryId() != 0) {
				ps.setInt(idx++, se.getCategoryId());
			}
			if (se.getProductName().length() != 0) {
				ps.setString(idx++, se.getProductName());
			}
			if (se.getSaleRemarks().length() != 0) {
				ps.setString(idx++, se.getSaleRemarks());
			}
			rs = ps.executeQuery();
			if (!rs.next()) {
				dataErrors.add("検索結果はありません");
			}
		} catch (Exception e) {
		} finally {
			try {
				if (ps != null) {
					ps.close();
				}
				if (rs != null) {
					rs.close();
				}
			} catch (Exception e) {
			}
		}
		return dataErrors;
	}

	List<S0021> getList(Connection con, S0020search se) {
		List<S0021> result = new ArrayList<>();

		PreparedStatement ps = null;
		ResultSet rs = null;
		try {
			int idx = 1;
			StringBuilder sql = new StringBuilder();
			sql.append("select ");
			sql.append("	s.sale_id,  ");
			sql.append("	s.sale_date, ");
			sql.append("	c.charge_id, ");
			sql.append("	c.charge_name, ");
			sql.append("	cat.category_id, ");
			sql.append("	cat.category_name, ");
			sql.append("	s.product_name, ");
			sql.append("	s.product_price, ");
			sql.append("	s.product_count ");
			sql.append(" from sale s ");
			sql.append(" join charge c on( ");
			sql.append("	s.charge_id = c.charge_id ");
			sql.append(") ");
			sql.append(" join category cat on( ");
			sql.append("	s.category_id = cat.category_id ");
			sql.append(") ");
			sql.append("where 1 = 1 ");
			if (se.getFrom() != null && se.getTo() != null) {
				sql.append("and s.sale_date between ? and ? ");
			}
			if (se.getChargeId() != 0) {
				sql.append("and s.charge_id = ? ");
			}
			if (se.getCategoryId() != 0) {
				sql.append("and s.category_id = ? ");
			}
			if (se.getProductName().length() != 0) {
				sql.append("and s.product_name like ? ");
			}
			if (se.getSaleRemarks().length() != 0) {
				sql.append("and s.sale_remarks like ?");
			}
			ps = con.prepareStatement(sql.toString());
			if (se.getFrom() != null & se.getTo() != null) {
				ps.setDate(idx++, java.sql.Date.valueOf(se.getFrom()));
				ps.setDate(idx++, java.sql.Date.valueOf(se.getTo()));
			}
			if (se.getChargeId() != 0) {
				ps.setInt(idx++, se.getChargeId());
			}
			if (se.getCategoryId() != 0) {
				ps.setInt(idx++, se.getCategoryId());
			}
			if (se.getProductName().length() != 0) {
				ps.setString(idx++, "%" + se.getProductName() + "%");
			}
			if (se.getSaleRemarks().length() != 0) {
				ps.setString(idx++, "%" + se.getSaleRemarks() + "%");
			}
			rs = ps.executeQuery();
			//ps = con.prepareStatement("select * from sale where sale_date and charge_id and category_id and (product_name like ?) and (set_remarks like ?) order by sale_id");
			//rs = ps.executeQuery();
			while (rs.next()) {
				S0021 s = new S0021();
				s.setSaleId(rs.getInt("sale_id"));
				s.setSaleDate(rs.getDate("sale_date").toLocalDate());
				s.setChargeId(rs.getInt("charge_id"));
				s.setChargeName(rs.getString("charge_name"));
				s.setCategoryId(rs.getInt("category_id"));
				s.setCategoryName(rs.getString("category_name"));
				s.setProductName(rs.getString("product_name"));
				s.setPrice(rs.getInt("product_price"));
				s.setCount(rs.getInt("product_count"));
				s.setAmount(s.getPrice() * s.getCount());
				result.add(s);
			}
		} catch (Exception e) {
			e.printStackTrace();
		} finally {
			try {
				if (ps != null)
					ps.close();
			} catch (Exception ee) {
			}
		}
		return result;
	}

	void delete(Connection con, Sell s) {
		PreparedStatement ps = null;
		try {
			ps = con.prepareStatement("delete from sale where sale_id = ?");
			ps.setInt(1, s.getSaleId());
			ps.executeUpdate();
		} catch (Exception ee) {
			ee.printStackTrace();
		} finally {
			try {
				if (ps != null)
					ps.close();
			} catch (Exception ee) {
			}
		}
	}

	void update(Connection con, Sell s) {
		PreparedStatement ps = null;
		java.sql.Date sqlDate = java.sql.Date.valueOf(s.getSaleDate());
		try {
			ps = con.prepareStatement(
					"update sale set sale_date = ?, charge_id = ?, category_id = ?, product_name = ?, product_price = ?, product_count = ?, sale_remarks = ? where sale_id = ?");
			ps.setDate(1, sqlDate);
			ps.setInt(2, s.getChargeId());
			ps.setInt(3, s.getCategoryId());
			ps.setString(4, s.getProductName());
			ps.setInt(5, s.getProductPrice());
			ps.setInt(6, s.getProductCount());
			ps.setString(7, s.getSaleRemarks());
			ps.setInt(8, s.getSaleId());
			ps.executeUpdate();
		} catch (Exception ee) {
			ee.printStackTrace();
		} finally {
			try {
				if (ps != null)
					ps.close();
			} catch (Exception ee) {
			}
		}
	}


	S0021 getDetail(Connection con, Sell sell) {
		S0021 detail = new S0021();

		PreparedStatement ps = null;
		ResultSet rs = null;
		try {
			int idx = 1;
			StringBuilder sql = new StringBuilder();
			sql.append("select ");
			sql.append("	s.sale_id,  ");
			sql.append("	s.sale_date, ");
			sql.append("	c.charge_id, ");
			sql.append("	c.charge_name, ");
			sql.append("	cat.category_id, ");
			sql.append("	cat.category_name, ");
			sql.append("	s.product_name, ");
			sql.append("	s.product_price, ");
			sql.append("	s.product_count, ");
			sql.append("    s.sale_remarks ");
			sql.append(" from sale s ");
			sql.append(" join charge c on( ");
			sql.append("	s.charge_id = c.charge_id ");
			sql.append(") ");
			sql.append(" join category cat on( ");
			sql.append("	s.category_id = cat.category_id ");
			sql.append(") ");
			sql.append("where s.sale_id = ? ");

			ps = con.prepareStatement(sql.toString());
			ps.setInt(1, sell.getSaleId());
			rs = ps.executeQuery();
			if (rs.next()) {

				detail.setSaleId(rs.getInt("sale_id"));
				detail.setSaleDate(rs.getDate("sale_date").toLocalDate());
				detail.setChargeId(rs.getInt("charge_id"));
				detail.setChargeName(rs.getString("charge_name"));
				detail.setCategoryId(rs.getInt("category_id"));
				detail.setCategoryName(rs.getString("category_name"));
				detail.setProductName(rs.getString("product_name"));
				detail.setPrice(rs.getInt("product_price"));
				detail.setCount(rs.getInt("product_count"));
				detail.setSaleRemarks(rs.getString("sale_remarks"));
				detail.setAmount(detail.getPrice() * detail.getCount());

			}
		} catch (Exception e) {
			e.printStackTrace();
		} finally {
			try {
				if (ps != null)
					ps.close();
			} catch (Exception ee) {
			}
		}
		return detail;
	}

	List<String[]> getCharges(Connection con) {
		List<String[]> res = new ArrayList<>();
		PreparedStatement ps = null;
		ResultSet rs = null;
		try {

			ps = con.prepareStatement("select * from charge order by charge_name");
			rs = ps.executeQuery();
			while (rs.next()) {

				res.add(new String[] { rs.getString("charge_id"), rs.getString("charge_name") });
			}
		} catch (Exception e) {
			e.printStackTrace();
		} finally {
			try {
				if (ps != null)
					ps.close();
			} catch (Exception ee) {
			}
		}
		return res;

	}

	List<String[]> getCategories(Connection con) {
		List<String[]> res = new ArrayList<>();
		PreparedStatement ps = null;
		ResultSet rs = null;
		try {

			ps = con.prepareStatement("select * from category order by category_name");
			rs = ps.executeQuery();
			while (rs.next()) {

				res.add(new String[] { rs.getString("category_id"), rs.getString("category_name") });
			}
		} catch (Exception e) {
			e.printStackTrace();
		} finally {
			try {
				if (ps != null)
					ps.close();
			} catch (Exception ee) {
			}
		}
		return res;

	}

}
