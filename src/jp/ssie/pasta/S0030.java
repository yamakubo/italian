package jp.ssie.pasta;

import lombok.Data;

@Data
public class S0030 {
	private int chargeId;
	private String chargeName;
	private String chargeMail;;
	private String chargePassword;
	private String chargePassConfirm;
	private int chargeAuthLevel;

}
