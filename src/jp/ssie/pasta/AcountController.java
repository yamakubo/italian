package jp.ssie.pasta;

import java.io.IOException;
import java.sql.Connection;
import java.util.ArrayList;
import java.util.List;

import javax.naming.InitialContext;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import javax.sql.DataSource;

@WebServlet("/acount")
public class AcountController extends HttpServlet {
	private static final long serialVersionUID = 1L;

	protected void doGet(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {
		//ログイン情報がないとき、ログイン画面に強制送還。
		HttpSession session = request.getSession();
		List<String> lu = (List<String>) session.getAttribute("login");
		if (lu == null) {
			response.sendRedirect("C0010");
			return;
		}

		request.setCharacterEncoding("UTF-8");
		AcountService service = new AcountService();
		Connection con = getConnection();
		String pg = request.getParameter("pg");
		String jsp = "/S0030.jsp";

		//アカウント検索条件入力
		if (pg != null && pg.equals("S0040")) {

		}

		//アカウント検索結果表示
		if (pg != null && pg.equals("S0041")) {
			jsp = "/S0041.jsp";

			S0040serach se = new S0040serach();
			Acount ac = new Acount();
			se.setChargeName((request.getParameter("chargeName")));
			se.setChargeMail((request.getParameter("chargeMail")));
			se.setChargePassword((request.getParameter("chargePassword")));
			se.setChargeAuthLevel(Integer.parseInt(request.getParameter("chargeAuthLevel")));
			request.setAttribute("list", service.getList(con, se));
			jsp = "/S0041.jsp";

			System.out.println(se);

			ac.setChargeName(request.getParameter("chargeName"));
			ac.setChargeMail(request.getParameter("chargeMail"));
			ac.setChargePassword(request.getParameter("chargePassword"));

			List<String> searches = new ArrayList<String>();

			searches.add(request.getParameter("chargeName"));
			searches.add(request.getParameter("chargeMail"));
			searches.add(request.getParameter("chargeAuthLevel"));
			session.setAttribute("searches", searches);

			List<String> errors = service.searchvalidation(ac);
			if (errors.size() != 0) {
				request.setAttribute("errors", errors);
				jsp = "/S0040.jsp";
				getServletContext().getRequestDispatcher(jsp).forward(request, response);
				return;
			} else {
				jsp = "/S0041.jsp";
			}
		}
		if (pg != null && pg.equals("S0042")) {
			jsp = "/S0042.jsp";
			Acount acount = new Acount();
			String chargeId = request.getParameter("charge_id");
			acount.setChargeId(Integer.parseInt(chargeId));
			request.setAttribute("bean", service.getDetail(con, acount));

		}
		if (pg != null && pg.equals("S0043")) {
			jsp = "/S0043.jsp";
			Acount acount = new Acount();
			String chargeId = request.getParameter("chargeId");
			acount.setChargeId(Integer.parseInt(chargeId));
			service.update(con, acount);

		}

		//削除確認画面
		if (pg != null && pg.equals("S0044")) {
			jsp = "/S0044.jsp";
			Acount acount = new Acount();

			String chargeId = request.getParameter("charge_id");
			acount.setChargeId(Integer.parseInt(chargeId));
			request.setAttribute("bean", service.getDetail(con, acount));

			String deleteId = request.getParameter("charge_id");
			if (deleteId != null) {
				Acount a = new Acount();
				a.setChargeId(Integer.parseInt(deleteId));
				service.delete(con, a);

			}
		}
		try {
			con.close();
		} catch (Exception e) {

			e.printStackTrace();
		}

		getServletContext().getRequestDispatcher(jsp).forward(request, response);
	}

	protected void doPost(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {
		request.setCharacterEncoding("UTF-8");
		HttpSession session = request.getSession();
		List<String> lu = (List<String>) session.getAttribute("login");
		if (lu == null) {
			response.sendRedirect("C0010");
			return;
		}

		String pg = request.getParameter("pg");
		String jsp = "/S0030.jsp";
		AcountService service = new AcountService();
		Connection con = getConnection();

		if (pg != null && pg.equals("S0031")) {
			Acount ac = new Acount();
			ac.setChargeName(request.getParameter("chargeName"));
			ac.setChargeMail(request.getParameter("chargeMail"));
			ac.setChargePassword(request.getParameter("chargePassword"));
			ac.setChargePassConfirm(request.getParameter("chargePassConfirm"));

			try {
				ac.setChargeAuthLevel(Integer.parseInt(request.getParameter("chargeAuthLevel")));
			} catch (Exception e) {

			}

			List<String> errors = service.validation(ac, con);
			if (errors.size() != 0) {
				request.setAttribute("errors", errors);
				jsp = "/S0030.jsp";
				getServletContext().getRequestDispatcher(jsp).forward(request, response);
				return;
			} else {
				jsp = "/S0031.jsp";
			}
		}

		if (pg != null && pg.equals("S0030")) {
			Acount a = new Acount();
			a.setChargeName(request.getParameter("chargeName"));
			a.setChargeMail(request.getParameter("chargeMail"));
			a.setChargePassword(request.getParameter("chargePassword"));
			a.setChargePassConfirm(request.getParameter("chargePassConfirm"));
			a.setChargeAuthLevel(Integer.parseInt(request.getParameter("chargeAuthLevel")));
			//			a.setCharge_delete_flg(Integer.parseInt(request.getParameter("charge_delete_flg")));

			service.regist(con, a);
			response.sendRedirect("acount");
			return;

		}

		if (pg != null && pg.equals("S0041")) {
			jsp = "/S0041.jsp";

			String deleteId = request.getParameter("deleteId");
			if (deleteId != null) {
				Acount ac = new Acount();
				ac.setChargeId(Integer.parseInt(deleteId));
				ac.setChargeName(request.getParameter("chargeName"));
				ac.setChargeMail(request.getParameter("chargeMail"));
				ac.setChargePassword(request.getParameter("chargePassword"));
				ac.setChargeAuthLevel(Integer.parseInt(request.getParameter("chargeAuthLevel")));

				service.delete(con, ac);

			}

			String updateId = request.getParameter("updateId");
			if (updateId != null) {
				Acount se = new Acount();

				S0040serach s = new S0040serach();
				se.setChargeId(Integer.parseInt(updateId));
				se.setChargeName(request.getParameter("chargeName"));
				se.setChargeMail(request.getParameter("chargeMail"));
				se.setChargePassword(request.getParameter("chargePassword"));
				se.setChargeAuthLevel(Integer.parseInt(request.getParameter("chargeAuthLevel")));
				request.setAttribute("list", service.getList(con, s));

				service.update(con, se);
			}
			List<String> searches = (List<String>) session.getAttribute("searches");
			//		String chargeId = searches.get(0);
			String chargeName = searches.get(0);
			String chargeMail = searches.get(1);
			String chargeAuthLevel = searches.get(2);

			response.sendRedirect(
					"acount?pg=S0041&chargeName=" + chargeName + "&chargeMail=" + chargeMail
							+ "&chargeAuthLevel=" + chargeAuthLevel);
			return;
		}

		if (pg != null && pg.equals("S0042")) {
			jsp = "/S0042.jsp";

			Acount a = new Acount();
			a.setChargeName(request.getParameter("chargeName"));
			a.setChargeMail(request.getParameter("chargeMail"));
			a.setChargePassword(request.getParameter("chargePassword"));
			a.setChargeAuthLevel(Integer.parseInt(request.getParameter("chargeAuthLevel")));
			service.update(con, a);

			try {
				con.close();
			} catch (Exception e) {
				e.printStackTrace();
			}

		}
		if (pg != null && pg.equals("S0043")) {
			Acount ac = new Acount();
			jsp = "/S0043.jsp";

			String chargeId = request.getParameter("charge_id");
			ac.setChargeId(Integer.parseInt(chargeId));
			ac.setChargeName(request.getParameter("chargeName"));
			ac.setChargeMail(request.getParameter("chargeMail"));
			ac.setChargePassword(request.getParameter("chargePassword"));
			ac.setChargeAuthLevel(Integer.parseInt(request.getParameter("chargeAuthLevel")));

			request.setAttribute("bean", service.getDetail(con, ac));

			try {
				con.close();
			} catch (Exception e) {
				e.printStackTrace();
			}

		}

		this.getServletContext().getRequestDispatcher(jsp).forward(request, response);
	}

	private Connection getConnection() throws ServletException {
		Connection con = null;
		try {
			DataSource ds = (DataSource) new InitialContext().lookup("java:/comp/env/jdbc/pasta");
			con = ds.getConnection();
		} catch (Exception e) {
			throw new ServletException(e);
		}
		return con;
	}

}
