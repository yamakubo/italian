package jp.ssie.pasta;

import java.io.IOException;
import java.sql.Connection;
import java.util.List;

import javax.naming.InitialContext;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import javax.sql.DataSource;

/**
 * Servlet implementation class S0010Controller
 */
@WebServlet("/S0010")
public class S0010Controller extends HttpServlet {
	private static final long serialVersionUID = 1L;

	protected void doGet(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {
		//ログイン情報がないとき、ログイン画面に強制送還。
				HttpSession session = request.getSession();
				List<String> lu = (List<String>) session.getAttribute("login");
				if (lu == null) {
					response.sendRedirect("C0010");
					return;
				}


		S0010Service service = new S0010Service();
		Connection con = getConnection();
		String pg = request.getParameter("pg");
		String jsp = "/S0010.jsp";
		request.setAttribute("charges", service.getCharges(con));
		request.setAttribute("categories", service.getCategories(con));
		if (pg != null && pg.equals("S0011")) {
			jsp = "/S0011.jsp";
		}
		request.getRequestDispatcher("/S0010.jsp").forward(request, response);

		try {
			con.close();
		} catch (Exception e) {
			// TODO 自動生成された catch ブロック
			e.printStackTrace();
		}
		//doGet(request, response);

	}





	protected void doPost(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {
		request.setCharacterEncoding("UTF-8");
		String pg = request.getParameter("pg");
		String jsp = "/C0010.jsp";
		S0010Service service = new S0010Service();
		Connection con = getConnection();
		request.setAttribute("charges", service.getCharges(con));
		request.setAttribute("categories", service.getCategories(con));
		if (pg != null && pg.equals("S0010")) {
			jsp ="/S0010.jsp";
			HttpSession session = request.getSession();
			if (session == null) {
				jsp = "/C0010.jsp";
			}
			Sell s = new Sell();
			s.setSaleDate(request.getParameter("saleDate"));
			s.setChargeId(Integer.parseInt(request.getParameter("chargeId")));
			s.setCategoryId(Integer.parseInt(request.getParameter("categoryId")));
			s.setProductName(request.getParameter("productName"));
			s.setProductPrice(Integer.parseInt(request.getParameter("productPrice")));
			s.setProductCount(Integer.parseInt(request.getParameter("productCount")));
			s.setSaleRemarks(request.getParameter("saleRemarks"));


			service.regist(con, s);
			response.sendRedirect("S0010");

		}


		if (pg != null && pg.equals("S0011")) {
			Sell se = new Sell();
			try {
				se.setSaleDate(request.getParameter("saleDate"));
				se.setChargeId(Integer.parseInt(request.getParameter("chargeId")));
			} catch (Exception e) {

			}
			try {
				se.setCategoryId(Integer.parseInt(request.getParameter("categoryId")));
			} catch (Exception e) {

			}
			se.setProductName(request.getParameter("productName"));
			try {
				se.setProductPrice(Integer.parseInt(request.getParameter("productPrice")));
			} catch (Exception e) {

			}
			try {
				se.setProductCount(Integer.parseInt(request.getParameter("productCount")));
			} catch (Exception e) {

			}

			se.setSaleRemarks(request.getParameter("saleRemarks"));


			List<String> errors = service.validation(se, con);
			if (errors.size() != 0) {
				request.setAttribute("errors", errors);
				jsp = "/S0010.jsp";
				getServletContext().getRequestDispatcher(jsp).forward(request, response);
				return;
			} else {
				jsp = "/S0011.jsp";
				getServletContext().getRequestDispatcher(jsp).forward(request, response);
			}
		}

		try {
			con.close();
		} catch (Exception e) {
			// TODO 自動生成された catch ブロック
			e.printStackTrace();
		}
		//doGet(request, response);

	}

	private Connection getConnection() throws ServletException {
		Connection con = null;
		try {
			DataSource ds = (DataSource) new InitialContext().lookup("java:/comp/env/jdbc/pasta");
			con = ds.getConnection();
		} catch (Exception e) {
			throw new ServletException(e);
		}
		return con;
	}

}
